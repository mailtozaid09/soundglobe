const clientId = 'a7f6767b839a434fb9b7f9e07e0ec0cb';
const redirectUri = 'http://localhost:5173/callback';

export const getAuthUrl = () => {

    const scopes = ['user-read-private', 'user-read-email', 'user-top-read'];
    const url = `https://accounts.spotify.com/authorize?response_type=token&client_id=${clientId}&redirect_uri=${encodeURIComponent(redirectUri)}&scope=${scopes.join('%20')}`;
    return url;
};


export const getTokenFromUrl = () => {
    const hash = window.location.hash.substring(1); 
    const params = new URLSearchParams(hash); 
    const token = params.get('access_token'); 
    const expires_in = params.get('expires_in'); 
  
    return token ? { access_token: token, expires_in } : null; 
  };
  