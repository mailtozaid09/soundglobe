import React, { useEffect } from 'react';
import { useNavigate } from 'react-router-dom';
import { getTokenFromUrl } from '../../services/authService';

const Callback = ({ onLogin }) => {
  const navigate = useNavigate();

  useEffect(() => {
    const token = getTokenFromUrl(); // Get token from URL after Spotify authentication
    if (token) {

        console.log('token>> ',token);
        console.log("accc > ", token?.access_token);
    

      localStorage.setItem('spotify_token', token?.access_token); // Store token in localStorage
      onLogin(); // Callback to update state in App.js
      navigate('/home'); // Navigate to home page after successful login
    } else {
      //navigate('/login'); // Navigate to login page if no token received
    }
  }, [navigate, onLogin]);

  return (
    <div>
      <h1>Logging in...</h1>
    </div>
  );
};

export default Callback;
