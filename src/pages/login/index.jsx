import React from 'react';
import { getAuthUrl } from '../../services/authService';
import './styles.css';
import globe from '../../assets/globe.png';
import spotify from '../../assets/spotify.png';

const Login = () => {
  const handleLogin = () => {
    window.location.href = getAuthUrl();
  };

  return (
    <div className="login-container">
        <div className="login-left">
            <h1 className="login-heading">Which country are your fav artists from?</h1>
            <p className="login-subheading">Sync your Spotify and get a map of where the artists you’re into are from</p>
            <button className="login-button" onClick={handleLogin}>
                <img src={spotify} alt="Globe" className="spotify-image" />
                <h1 className='button_title' >Connect Spotify</h1>
            </button>
        </div>
        <div className="login-right">
            <img src={globe} alt="Globe" className="login-image" />
        </div>
    </div>
  );
};

export default Login;
