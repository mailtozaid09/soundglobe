// src/App.js
import React, { useState } from 'react';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import Login from './pages/login';
import Home from './pages/home';
import Callback from './pages/callback';

const App = () => {
  const [isLoggedIn, setIsLoggedIn] = useState(false);

  const handleLogin = () => {
    setIsLoggedIn(true);
  };

  const handleLogout = () => {
    setIsLoggedIn(false);
  };

  return (
    <Router>
      <Routes>
        <Route path="/login" element={<Login />} />
        <Route path="/home" element={<Home onLogout={handleLogout} />} />
        <Route path="/callback" element={<Callback onLogin={handleLogin} />} />
     
        <Route path="*" element={<Login />} />
      </Routes>
    </Router>
  );
};

export default App;
