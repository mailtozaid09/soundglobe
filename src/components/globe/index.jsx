import React, { useEffect, useState, useRef } from 'react';
import Globe from 'react-globe.gl';
import axios from 'axios';
import { useNavigate } from 'react-router-dom';
import './styles.css'; 

const GlobeView = () => {
  const [token, setToken] = useState(null);
  const [artistsData, setArtistsData] = useState([]);
  const globeRef = useRef();
  const navigate = useNavigate();

  useEffect(() => {
    const accessToken = localStorage.getItem('spotify_token');
    if (!accessToken) {
      navigate('/login');
    }
    setToken(accessToken);
    fetchData();
  }, [navigate]);


  const handleLogout = () => {
    localStorage.removeItem('spotify_token');

    navigate('/login'); 
  };

  const fetchArtistCountry = async (artistName) => {
    try {
      const searchResponse = await axios.get('https://cors-anywhere.herokuapp.com/https://musicbrainz.org/ws/2/artist', {
        params: {
          query: `artist:${artistName}`,
          fmt: 'json'
        }
      });

      const artists = searchResponse.data.artists;

      if (!artists || artists.length === 0) {
        throw new Error(`Artist '${artistName}' not found on MusicBrainz.`);
      }

      const mbid = artists[0].id;

      const artistResponse = await axios.get(`https://cors-anywhere.herokuapp.com/https://musicbrainz.org/ws/2/artist/${mbid}`, {
        params: {
          fmt: 'json'
        }
      });

      const country = artistResponse.data.country;
      return { name: artistName, country };
    } catch (error) {
      console.error(`Error fetching data for artist '${artistName}' from MusicBrainz:`, error);
      return { name: artistName, country: 'Unknown' };
    }
  };

  const fetchArtists = async () => {
    try {
      const accessToken = localStorage.getItem('spotify_token');
      if (!accessToken) {
        console.error('No access token found.');
        return [];
      }

      const response = await axios.get('https://api.spotify.com/v1/me/top/artists', {
        headers: {
          'Authorization': `Bearer ${accessToken}`
        },
        params: {
          time_range: 'medium_term',
          limit: 50
        }
      });

      const artistNames = response.data.items.map(artist => artist.name);

      return artistNames;
    } catch (error) {
      console.error('Error fetching data from Spotify:', error);
      return [];
    }
  };

  const fetchData = async () => {
    try {
      const artistNames = await fetchArtists();
      const artistsWithCountries = await Promise.all(artistNames.map(async (artistName) => {
        return await fetchArtistCountry(artistName);
      }));

      setArtistsData(artistsWithCountries);
    } catch (error) {
      console.error('Error fetching artist data:', error);
    }
  };

  const captureGlobeView = () => {
    const canvas = globeRef.current.querySelector('canvas');
    return canvas.toDataURL('image/png');
  };

  const handleShare = async () => {
    console.log('handleShare>> ');
    try {
      const imageData = captureGlobeView();
      const response = await axios.post('http://localhost:3001/save-image', { imageData });
      const shareUrl = response.data.url;
      const twitterShareUrl = `https://twitter.com/intent/tweet?url=${encodeURIComponent(shareUrl)}&text=${encodeURIComponent('Check out my music map!')}`;

      window.open(twitterShareUrl, '_blank');
    } catch (error) {
      console.error('Error sharing image:', error);
    }
  };

  const pointsData = artistsData.map(artist => ({
    latitude: Math.random() * 180 - 90,
    longitude: Math.random() * 360 - 180,
    name: artist.name,
    country: artist.country
  }));

  return (
    <div className="container">
      <div className="globe-container" ref={globeRef}>
        <Globe
          globeImageUrl="https://unpkg.com/three-globe/example/img/earth-blue-marble.jpg"
          labelsData={pointsData}
          labelLat={d => d.latitude}
          labelLng={d => d.longitude}
          labelText={d => d.name}
          labelSize={3}
          labelDotRadius={1}
          labelColor={() => 'rgba(255, 165, 0, 0.75)'}
          labelResolution={2}
        />
      </div>

      <h1 className='globe_heading'>Your music map</h1>

      <div className='bottom_container'>
        <button className="share-button" onClick={handleShare}>Share with friends</button>
        <h1 onClick={handleLogout} className='logout'>Logout</h1>
      </div>

    </div>
  );
};

export default GlobeView;
